<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'DashboardController@index')->name('dashboard');

Route::prefix('projects')->group(function () {
    Route::get('/', 'ProjectsController@index');
    Route::get('/get/{project}', 'ProjectsController@get');
    Route::delete('/delete/{project}', 'ProjectsController@delete');
    Route::get('/view/{project}', 'ProjectsController@view');
    Route::get('/load', 'ProjectsController@getProjectsLists');
    Route::any('/create', 'ProjectsController@create');
    Route::any('/update/{project?}', 'ProjectsController@update');
    Route::get('/create/load', 'ProjectsController@loadToCreate');
});

Route::prefix('deliverable')->group(function () {
    Route::delete('/delete/{deliverables}', 'DeliverableController@delete');
    Route::any('/create', 'DeliverableController@create');
    Route::any('/update/{deliverables?}', 'DeliverableController@update');
    Route::get('/create/load', 'DeliverableController@loadCreate');
    Route::get('/get/{deliverables}', 'DeliverableController@get');
});


Route::prefix('employees')->group(function () {
    Route::get('/', 'EmployeeController@index')->name('user');
    Route::get('/load', 'EmployeeController@getEmployeesList')->name('userLoad');
    Route::get('/create/getForm/{user?}', 'EmployeeController@getCreateForm')->name('userCreateForm');
    Route::post('/create', 'EmployeeController@create')->name('userCreate');
    Route::post('/update/{user}', 'EmployeeController@update')->name('userUpdate');
    Route::delete('/delete/{user}', 'EmployeeController@delete')->name('userDelete');
});

Route::prefix('roles')->group(function () {
    Route::get('/', 'RoleController@index')->name('role');
    Route::get('/load', 'RoleController@getRolesList')->name('roleLoad');
    Route::get('/create/getForm/{role?}', 'RoleController@getCreateForm')->name('roleCreateForm');
    Route::post('/create', 'RoleController@create')->name('roleCreate');
    Route::post('/update/{role}', 'RoleController@update')->name('roleUpdate');
    Route::delete('/delete/{role}', 'RoleController@delete')->name('roleDelete');
});