<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/19/17
 * Time: 8:42 PM
 */

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class RoleController
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('roles.index');
    }

    /**
     * @return JsonResponse
     */
    public function getRolesList()
    {
        $roles = Role::all();

        return response()->json(compact('roles'));
    }

    /**
     * @param Role|null $role
     * @return View
     */
    public function getCreateForm(Role $role = null)
    {
        return view('roles.create', compact('role'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $role = Role::create($request->all());

        return response()->json(compact('role'));
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return JsonResponse
     */
    public function update(Request $request, Role $role)
    {
        $role->update($request->all());

        return response()->json(compact('role'));
    }

    /**
     * @param Role $role
     * @return JsonResponse
     */
    public function delete(Role $role)
    {
        $role->delete();

        return response()->json(true);
    }
}