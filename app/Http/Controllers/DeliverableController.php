<?php

namespace App\Http\Controllers;

use App\Models\Deliverables;
use App\Models\Level;
use Illuminate\Http\Request;

/**
 * Class DeliverableController
 * @package App\Http\Controllers
 */
class DeliverableController extends Controller
{
    /**
     * @return array
     */
    public function loadCreate()
    {
        $levels = Level::all();

        return compact('levels');
    }

    /**
     * @param Request $request
     * @param Deliverables|null $deliverables
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, Deliverables $deliverables = null)
    {
        if ($request->method() === 'POST') {
            $data = $request->all();
            unset($data['level']);

            $deliverables->update($data);

            return $this->getDeliverable($deliverables);
        } else {
            return view('deliverable.create');
        }
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($request->method() === 'POST') {
            $data = $request->all();

            $deliverables = Deliverables::create($data);

            return compact('deliverables');
        } else {
            return view('deliverable.create');
        }
    }

    /**
     * @param Deliverables $deliverables
     * @return array
     */
    public function get(Deliverables $deliverables)
    {
        $deliverable = Deliverables::where('id', $deliverables->id)
            ->with('level')
            ->first()
        ;

        return compact('deliverable');
    }

    /**
     * @param Deliverables $deliverables
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Deliverables $deliverables)
    {
        $deliverables->delete();

        return response()->json(true);
    }
}
