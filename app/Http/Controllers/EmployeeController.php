<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class EmployeeController
 * @package App\Http\Controllers
 */
class EmployeeController extends Controller
{
    /**
     * ResponsibleController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('employee.index');
    }

    /**
     * @return JsonResponse
     */
    public function getEmployeesList()
    {
        $employees = User::with('roles')->get();

        return response()->json(compact('employees'));
    }

    /**
     * @param User|null $user
     * @return View
     */
    public function getCreateForm(User $user = null)
    {
        $roles = Role::all();
        $user = User::with('roles')->where('id', $user->id)->first();

        return view('employee.create', compact('user', 'roles'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        $user->attachRole($data['role']);

        $user = User::with('roles')->where('id', $user->id)->first();

        return response()->json(compact('user'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $data = $request->all();

        if (isset($data['password'])) {
            $data['password'] = \Hash::make($data['password']);
        }

        $user->update($data);

        $user->roles()->detach();
        $user->roles()->attach($data['role']);

        $user = User::with('roles')->where('id', $user->id)->first();

        return response()->json(compact('user'));
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function delete(User $user)
    {
        $user->delete();

        return response()->json(true);
    }
}
