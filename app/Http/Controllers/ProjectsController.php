<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Deliverables;
use App\Models\Priority;
use App\Models\Project;
use App\Models\Status;
use App\Models\Zone;
use App\Models\Level;
use App\User;
use Carbon\Carbon;
use Illuminate\View\View;
use Illuminate\Http\Request;

/**
 * Class ProjectsController
 * @package App\Http\Controllers
 */
class ProjectsController extends Controller
{
    /**
     * ProjectsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function index()
    {
        return view('projects.index');
    }

    /**
     * @return View
     */
    public function view()
    {
        return view('projects.view');
    }

    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Project $project)
    {
        $project->delete();

        return response()->json(true);
    }

    /**
     * @param Project $project
     * @return array
     */
    public function get(Project $project)
    {
        $project = Project::where('id', $project->id)
            ->with('priority')
            ->with('status')
            ->with('category')
            ->with('zone')
            ->with('manager')
            ->with('deliverables')
            ->with('deliverables.level')
            ->first()
        ;

        return compact('project');
    }

    /**
     * @return array
     */
    public function getProjectsLists()
    {
        $categories = Category::with('projects')
            ->with('projects.priority')
            ->with('projects.status')
            ->with('projects.category')
            ->with('projects.zone')
            ->with('projects.manager')
            ->get()
        ;

        return compact('categories');
    }

    /**
     * @param Request $request
     * @return array|View
     */
    public function create(Request $request)
    {
        if ($request->method() === 'POST') {
            $data = $request->all();

            $project = Project::create($data);

            return compact('project');
        } else {
            return view('projects.create');
        }
    }

    /**
     * @param Request $request
     * @param Project|null $project
     * @return array|View
     */
    public function update(Request $request, Project $project = null)
    {
        if ($request->method() === 'POST') {
            $data = $request->all();
            unset($data['priority'], $data['status'], $data['category']);
            unset($data['zone'], $data['manager']);

            $project->update($data);

            return $this->get($project);
        } else {
            return view('projects.create');
        }
    }

    /**
     * @return array
     */
    public function loadToCreate()
    {
        $priorities = Priority::all();
        $categories = Category::all();
        $statuses   = Status::all();
        $zones      = Zone::all();
        $users      = User::all();

        return compact('priorities', 'categories', 'statuses', 'zones', 'users');
    }
}
