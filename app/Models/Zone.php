<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Zone
 * @package App\Models
 */
class Zone extends Model
{
    use ProjectTrait;

    protected $table = 'zones';
}
