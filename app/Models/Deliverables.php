<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Deliverables
 * @package App\Models
 */
class Deliverables extends Model
{
    /**
     * @var string
     */
    protected $table = 'deliverables';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'description',
        'due_date', 'forecast_date', 'completed_date',
        'project_id', 'level_id'
    ];

    /**
     * @param $value
     */
    public function setProjectIdAttribute($value)
    {
        $this->attributes['project_id'] = $value;
    }

    /**
     * @param $value
     */
    public function setLevelIdAttribute($value)
    {
        $this->attributes['level_id'] = $value;
    }

    /**
     * @param $value
     */
    public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = Carbon::parse($value);
    }

    /**
     * @param $value
     */
    public function setForecastDateAttribute($value)
    {
        $this->attributes['forecast_date'] = Carbon::parse($value);
    }

    /**
     * @param $value
     */
    public function setCompletedDateAttribute($value)
    {
        $this->attributes['completed_date'] = Carbon::parse($value);
    }

    /**
     * @return BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    /**
     * @return BelongsTo
     */
    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'level_id');
    }
}
