<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 8/7/17
 * Time: 12:58 PM
 */

namespace App\Models;

/**
 * Class ProjectTrait
 * @package Models
 */
trait ProjectTrait
{
    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}