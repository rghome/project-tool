<?php
/**
 * Created by PhpStorm.
 * User: romangorbatko
 * Date: 7/19/17
 * Time: 7:24 PM
 */

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 * @package App\Models
 */
class Permission extends EntrustPermission
{
}