<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Priority
 * @package App\Models
 */
class Priority extends Model
{
    use ProjectTrait;

    protected $table = 'priorities';
}
