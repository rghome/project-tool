<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Carbon\Carbon;

/**
 * Class Project
 * @package App\Models
 */
class Project extends Model
{
    /**
     * @var string
     */
    protected $table = 'projects';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'number', 'title', 'priority_id', 'status_id',
        'due_date', 'forecast_date', 'completed_date',
        'zone_id', 'category_id', 'manager_id'
    ];

    /**
     * @param $value
     */
    public function setPriorityIdAttribute($value)
    {
        $this->attributes['priority_id'] = ($value['id']) ?: $value;
    }

    /**
     * @param $value
     */
    public function setStatusIdAttribute($value)
    {
        $this->attributes['status_id'] = ($value['id']) ?: $value;
    }

    /**
     * @param $value
     */
    public function setZoneIdAttribute($value)
    {
        $this->attributes['zone_id'] = ($value['id']) ?: $value;
    }

    /**
     * @param $value
     */
    public function setCategoryIdAttribute($value)
    {
        $this->attributes['category_id'] = ($value['id']) ?: $value;
    }

    /**
     * @param $value
     */
    public function setManagerAttribute($value)
    {
        $this->attributes['manager_id'] = ($value['id']) ?: $value;
    }

    /**
     * @param $value
     */
    public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = Carbon::parse($value);
    }

    /**
     * @param $value
     */
    public function setForecastDateAttribute($value)
    {
        $this->attributes['forecast_date'] = Carbon::parse($value);
    }

    /**
     * @param $value
     */
    public function setCompletedDateAttribute($value)
    {
        $this->attributes['completed_date'] = Carbon::parse($value);
    }

    /**
     * @return BelongsTo
     */
    public function priority()
    {
        return $this->belongsTo('App\Models\Priority', 'priority_id');
    }

    /**
     * @return BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    /**
     * @return BelongsTo
     */
    public function zone()
    {
        return $this->belongsTo('App\Models\Zone', 'zone_id');
    }

    /**
     * @return BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User', 'manager_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliverables()
    {
        return $this->hasMany('App\Models\Deliverables');
    }

    /**
     * Delete relations
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($project) { // before delete() method call this
            $project->deliverables()->delete();
            // do the rest of the cleanup...
        });
    }
}
