SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `projects-tool`.`zones`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `projects-tool`.`zones` ;

CREATE  TABLE IF NOT EXISTS `projects-tool`.`zones` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projects-tool`.`categorys`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `projects-tool`.`categorys` ;

CREATE  TABLE IF NOT EXISTS `projects-tool`.`categorys` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projects-tool`.`projects`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `projects-tool`.`projects` ;

CREATE  TABLE IF NOT EXISTS `projects-tool`.`projects` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(255) NOT NULL ,
  `priority` TINYINT NOT NULL ,
  `status` TINYINT NOT NULL ,
  `due_date` DATE NOT NULL ,
  `forecast_date` DATE NOT NULL ,
  `completed_date` DATE NOT NULL ,
  `zone_id` INT NULL ,
  `category_id` INT NULL ,
  `total_cost` DECIMAL(15,2) NULL ,
  `hours` DECIMAL(15,2) NULL ,
  `zones_id` INT NOT NULL ,
  `categorys_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `zones_id`, `categorys_id`) ,
  INDEX `fk_projects_categorys1_idx` (`categorys_id` ASC) ,
  INDEX `fk_projects_zones_idx` (`zones_id` ASC) ,
  CONSTRAINT `fk_projects_zones`
    FOREIGN KEY (`zones_id` )
    REFERENCES `projects-tool`.`zones` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_projects_categorys1`
    FOREIGN KEY (`categorys_id` )
    REFERENCES `projects-tool`.`categorys` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projects-tool`.`tasks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `projects-tool`.`tasks` ;

CREATE  TABLE IF NOT EXISTS `projects-tool`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `completed_date` DATE NOT NULL ,
  `due_date` DATE NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `Instruction` TEXT NULL DEFAULT NULL ,
  `project_id` INT NOT NULL ,
  `deliverable_id` INT NOT NULL ,
  `employee_id` INT NOT NULL ,
  `projects_id` INT NOT NULL ,
  `projects_zones_id` INT NOT NULL ,
  `projects_categorys_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `projects_id`, `projects_zones_id`, `projects_categorys_id`) ,
  INDEX `fk_tasks_projects1_idx` (`projects_id` ASC, `projects_zones_id` ASC, `projects_categorys_id` ASC) ,
  CONSTRAINT `fk_tasks_projects1`
    FOREIGN KEY (`projects_id` , `projects_zones_id` , `projects_categorys_id` )
    REFERENCES `projects-tool`.`projects` (`id` , `id` , `id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projects-tool`.`teams`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `projects-tool`.`teams` ;

CREATE  TABLE IF NOT EXISTS `projects-tool`.`teams` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `project_id` INT NOT NULL ,
  `projects_id` INT NOT NULL ,
  `projects_zones_id` INT NOT NULL ,
  `projects_categorys_id` INT NOT NULL ,
  `role` TINYINT NOT NULL ,
  `employee_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `projects_id`, `projects_zones_id`, `projects_categorys_id`) ,
  INDEX `fk_teams_projects1_idx` (`projects_id` ASC, `projects_zones_id` ASC, `projects_categorys_id` ASC) ,
  CONSTRAINT `fk_teams_projects1`
    FOREIGN KEY (`projects_id` , `projects_zones_id` , `projects_categorys_id` )
    REFERENCES `projects-tool`.`projects` (`id` , `zones_id` , `categorys_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
