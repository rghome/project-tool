<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliverablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
        });

        Schema::create('deliverables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->date('due_date');
            $table->date('forecast_date');
            $table->date('completed_date');

            $table->unsignedInteger('project_id')
                ->nullable()
                ->index()
            ;

            $table->unsignedInteger('level_id')
                ->nullable()
                ->index()
            ;

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
            ;

            $table->foreign('level_id')
                ->references('id')
                ->on('levels')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverables');
    }
}
