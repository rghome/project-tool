<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->foreign('categorys_id', 'fk_projects_categorys1')->references('id')->on('categorys')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('zones_id', 'fk_projects_zones')->references('id')->on('zones')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function(Blueprint $table)
		{
			$table->dropForeign('fk_projects_categorys1');
			$table->dropForeign('fk_projects_zones');
		});
	}

}
