<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title');
			$table->tinyInteger('priority');
			$table->tinyInteger('status');
			$table->date('due_date');
			$table->date('forecast_date');
			$table->date('completed_date');
			$table->integer('zone_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->decimal('total_cost', 15)->nullable();
			$table->decimal('hours', 15)->nullable();
			$table->integer('zones_id')->index('fk_projects_zones_idx');
			$table->integer('categorys_id')->index('fk_projects_categorys1_idx');
			$table->primary(['id','zones_id','categorys_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
