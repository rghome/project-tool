<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('completed_date');
			$table->date('due_date');
			$table->string('title');
			$table->text('Instruction', 65535)->nullable();
			$table->integer('project_id')->index('fk_tasks_projects1');
			$table->integer('deliverable_id');
			$table->integer('employee_id');
			$table->primary(['id','project_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
