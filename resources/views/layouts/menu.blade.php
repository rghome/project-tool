<div class="panel-heading">Menu</div>

<div class="panel-body">
    <ul class="nav">
        <li {{ (Route::current()->getName() === '/' ? 'class=active' : '') }}>
            {{--<a href="{{ url('/') }}"></a>--}}
            <router-link to="/"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</router-link>
        </li>
        <li {{ (Route::current()->getName() === '/projects' ? 'class=active' : '') }}>
            {{--<a href="{{ url('/') }}"></a>--}}
            <router-link to="/projects"><i class="fa fa-list" aria-hidden="true"></i> Projects</router-link>
        </li>
        {{--<li {{ (Route::current()->getName() === '/projects' ? 'class=active' : '') }}>--}}
            {{--<a href="{{ url('/') }}"></a>--}}
            {{--<router-link to="/projects"><i class="fa fa-tasks" aria-hidden="true"></i> Tasks</router-link>--}}
        {{--</li>--}}
        {{--@if (Auth::user()->hasRole('admin'))--}}
            {{--<li {{ (Route::current()->getName() === 'sites' ? 'class=active' : '') }}>--}}
                {{--<a href="{{ url('sites') }}"><span class="glyphicon glyphicon-globe"></span> Сайты</a>--}}
                {{--<router-link to="/sites"><span class="glyphicon glyphicon-globe"></span> Сайты</router-link>--}}
            {{--</li>--}}
            {{--<li {{ (Route::current()->getName() === 'hold' ? 'class=active' : '') }}>--}}
                {{--<a href="{{ url('sites') }}"><span class="glyphicon glyphicon-globe"></span> Сайты</a>--}}
                {{--<router-link to="/hold"><span class="glyphicon glyphicon-alert"></span> Холд</router-link>--}}
            {{--</li>--}}
            {{--<li {{ (Route::current()->getName() === 'labels' ? 'class=active' : '') }}>--}}
                {{--<a href="{{ url('labels') }}"><span class="glyphicon glyphicon-tag"></span> Группы</a>--}}
                {{--<router-link to="/labels"><span class="glyphicon glyphicon-tag"></span> Метки</router-link>--}}
            {{--</li>--}}
            <li {{ (Route::current()->getName() === 'employees' ? 'class=active' : '') }}>
                <router-link to="/employees"><i class="fa fa-users" aria-hidden="true"></i> Employees</router-link>
            </li>
            <li {{ (Route::current()->getName() === 'roles' ? 'class=active' : '') }}>
                <router-link to="/roles"><i class="fa fa-user-plus" aria-hidden="true"></i> Roles</router-link>
            </li>
            {{--<li {{ (Route::current()->getName() === 'setting' ? 'class=active' : '') }}>--}}
                {{--<a href="{{ url('responsible') }}"><span class="glyphicon glyphicon-user"></span> Ответственные</a>--}}
                {{--<router-link to="/setting"><span class="glyphicon glyphicon-cog"></span> Настройки</router-link>--}}
            {{--</li>--}}
        {{--@endif--}}
    </ul>
</div>