<form>
    {{--{{dump($user)}}--}}
    @if (isset($user) && $user->id)
        <input type="hidden" required class="form-control" name="id" id="id" value="{{$user->id}}">
    @endif

    <div class="form-group">
        <label for="name" class="control-label">Login:</label>
        <input type="text" required class="form-control" name="name" id="name" @if (isset($user) && $user->name) value="{{$user->name}}" @endif>
    </div>
    <div class="form-group">
        <label for="email" class="control-label">Email:</label>
        <input type="email" required class="form-control" name="email" id="email" @if (isset($user) && $user->email) value="{{$user->email}}" @endif>
    </div>

    {{--@if (isset($user))--}}
        <div class="form-group">
            <label for="password" class="control-label">Password:</label>
            <input type="password" required class="form-control" name="password" id="password">
        </div>
    {{--@endif--}}

    <div class="form-group">
        <label for="role" class="control-label">Role:</label><br>
        <select class="select2" name="role" id="role">
            @foreach($roles as $role)
                <option @if (isset($user) && $user->roles[0]['id'] == $role->id) selected @endif value="{{$role->id}}">{{$role->display_name}}</option>
            @endforeach
        </select>
    </div>
</form>