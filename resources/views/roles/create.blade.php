<form>
    {{--{{dump($role)}}--}}
    @if (isset($role) && $role->id)
        <input type="hidden" required class="form-control" name="id" id="id" value="{{$role->id}}">
    @endif

    <div class="form-group">
        <label for="name" class="control-label">Name:</label>
        <input type="text" required class="form-control" name="name" id="name" @if (isset($role) && $role->name) value="{{$role->name}}" @endif placeholder="admin">
    </div>
    <div class="form-group">
        <label for="display_name" class="control-label">Display name:</label>
        <input type="text" required class="form-control" name="display_name" id="display_name" @if (isset($role) && $role->display_name) value="{{$role->display_name}}" @endif placeholder="Admin">
    </div>
    <div class="form-group">
        <label for="description" class="control-label">Description:</label>
        <textarea class="form-control" rows="5" name="description" id="description">{{ (isset($role) && $role->description) ? $role->description : '' }}</textarea>
    </div>
</form>