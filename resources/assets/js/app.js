
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'

window.Vue = require('vue');

var VueRouter = require('vue-router/dist/vue-router.js');
Vue.use(VueRouter);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const Projects  = Vue.component('projects', require('./components/projects/index.vue'));
const ProjectsCreate  = Vue.component('projects-create', require('./components/projects/create.vue'));
const ProjectsView  = Vue.component('projects-view', require('./components/projects/view.vue'));

const DeliverableCreate  = Vue.component('deliverable-create', require('./components/deliverable/create.vue'));

const DocumentCreate  = Vue.component('documents-create', require('./components/documents/create.vue'));
const Dashboard = Vue.component('dashboard', require('./components/dashboard.vue'));
const Employees = Vue.component('employee-index', require('./components/employee/index.vue'));
const Role = Vue.component('role-index', require('./components/roles/index.vue'));

const routes = [
    { path: '/', name: 'Dashboard', component: Dashboard },
    { path: '/projects', name: 'Projects', component: Projects },
    { path: '/projects/create', name: 'ProjectsCreate', component: ProjectsCreate },
    { path: '/projects/update/:project?', name: 'ProjectsUpdate', component: ProjectsCreate },
    { path: '/projects/view/:project', name: 'ProjectsView', component: ProjectsView },
    { path: '/deliverable/create', name: 'DeliverableCreate', component: DeliverableCreate },
    { path: '/deliverable/update/:deliverable', name: 'DeliverableUpdate', component: DeliverableCreate },
    { path: '/documents/create', name: 'DocumentsCreate', component: DocumentCreate },
    { path: '/documents/update/:deliverable', name: 'DocumentsUpdate', component: DocumentCreate },
    { path: '/employees', name: 'Employees', component: Employees },
    { path: '/roles', name: 'Role', component: Role },
];

const router = new VueRouter({
    routes, // short for routes: routes
    mode: 'history'
});


// If using mini-toastr, provide additional configuration
const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
};

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
    return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
}

Vue.use(VueNotifications, options);

const app = new Vue({
    router
}).$mount('#app');
